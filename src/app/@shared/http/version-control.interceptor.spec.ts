import { Type } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';

import { VersionControlInterceptor } from './version-control.interceptor';
import { tap } from 'rxjs/operators';

describe('VersionControlInterceptor', () => {
  let http: HttpClient;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: VersionControlInterceptor,
          multi: true,
        },
      ],
    });

    http = TestBed.inject(HttpClient);
    httpMock = TestBed.inject(HttpTestingController as Type<HttpTestingController>);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should prepent environment.serverUrl to request url for version.json', () => {
    http
      .get('/assets/version.json')
      .pipe(tap((data) => console.log(data)))
      .subscribe((version) => {
        expect(version).toBeFalsy();
      });

    httpMock.expectNone('/assets/version.json');
  });
});
