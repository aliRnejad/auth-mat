import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Version } from '../interfaces/version';
import * as localVersion from 'assets/version.json';
import { Logger } from '..';
import { MatSnackBar } from '@angular/material/snack-bar';
import { map } from 'lodash';

const log = new Logger('version control ~>');
const ReloadTime = 5;

@Injectable()
export class VersionControlInterceptor implements HttpInterceptor {
  private version: Version = {
    version: '1.0.0',
    main: 'what123',
    agent: 'agent1',
    client: 'client1',
    review: 'review1',
    tmt: 'tmt1',
  };
  private _loadedVersion: Version = localVersion as Version;
  private counter = ReloadTime;

  constructor(private snackBar: MatSnackBar) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (!this.isEqual()) {
      log.error('do something');
      this.reloadIn();
    }
    return next.handle(request);
  }
  reloadIn() {
    const interval = setInterval(() => {
      this.snackBar.open(`Making things better in ${this.counter}s`, '', {
        horizontalPosition: 'center',
        verticalPosition: 'top',
        duration: 1000,
      });
      this.counter--;
      if (this.counter < 0) {
        clearInterval(interval);
        location.reload();
      }
    }, 1000);
  }

  private isEqual(): boolean {
    const keys = map(this.version, (value, key) => {
      if (this._loadedVersion[key] !== value) {
        log.info(`${key}: ${this._loadedVersion[key]} != ${value}`);
        return key;
      }
      return null;
    });
    const found = keys.filter((value) => !!value);
    if (found.length) {
      return false;
    }
    log.info('Same version');
    return true;
  }

  private hashMatch() {}
}
