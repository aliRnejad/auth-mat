export interface Version {
  version: string;
  main?: string;
  agent?: string;
  client?: string;
  review?: string;
  tmt?: string;
}
