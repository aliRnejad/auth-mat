import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MaterialModule } from '@app/material.module';
import { LoaderComponent } from './loader/loader.component';
import { HttpClientModule } from '@angular/common/http';
import { OverlayLoaderComponent } from './overlay-loader/overlay-loader.component';
import { MainProgressBarComponent, ProgressBarService } from './main-progress-bar';

@NgModule({
  imports: [FlexLayoutModule, MaterialModule, HttpClientModule, TranslateModule, CommonModule],
  declarations: [LoaderComponent, OverlayLoaderComponent, MainProgressBarComponent],
  providers: [ ProgressBarService],
  exports: [LoaderComponent, FlexLayoutModule, MaterialModule, TranslateModule, CommonModule, OverlayLoaderComponent, MainProgressBarComponent],
})
export class SharedModule {}
