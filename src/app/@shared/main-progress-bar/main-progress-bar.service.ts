import { EventEmitter, Injectable } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, RouterEvent, NavigationStart } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { Logger } from '../logger.service';
// import { Logger } from '..';
// import { Transition, TransitionService, UIRouter } from '@uirouter/angular';

const log = new Logger('~~~~~~~')

export interface RouterTransitionStateVM {
  isRouteLoading: boolean;
  isFirstRouteLoading: boolean;
  // : Transition;
}

@Injectable({
  providedIn: 'root',
})
export class ProgressBarService {

  isLoadingManually: boolean = false;
  routerTransitionStateVM: RouterTransitionStateVM = { isFirstRouteLoading: true, isRouteLoading: true };
  onLoadingStateChanged = new EventEmitter(true);

  constructor(private router: Router, private activated: ActivatedRoute) {
    log.error(router.routerState.snapshot);
    router.events.pipe(
      filter((e: any) => e instanceof RouterEvent),
      // filter((e: RouterEvent) => e instanceof NavigationEnd)
    ).subscribe(route => {
      log.debug('route~>', route);
      if (route instanceof NavigationStart) {
        this.routerTransitionStateVM.isRouteLoading = true;
      }
      if (route instanceof NavigationEnd) {
        this.routerTransitionStateVM.isRouteLoading = false;
      }
      this.onLoadingStateChanged.next(this.routerTransitionStateVM);
   });
  }

  setLoadingManually(flag: boolean) {
    this.isLoadingManually = flag;
    this.updateLoadingStateChangedAndFire();
  }


  getLoadingStateVM() {
    return this.routerTransitionStateVM;
  }

  updateLoadingStateChangedAndFire(router?: Router) {

    // const i

    // const isLoadingUiRouter =  && .success === undefined;
    // const isLoadingUiRouterFirst =  && .from().name === '';
    // this.uiRouterTransitionStateVM = {
    //   isRouteLoading: !!(isLoadingUiRouter || this.isLoadingManually),
    //   isFirstRouteLoading: !!(isLoadingUiRouter && isLoadingUiRouterFirst),
    //   ,
    // };
    // this.onLoadingStateChanged.next(this.uiRouterTransitionStateVM);
  }

  // onLoadingStateChanged;
  // onAsyncLoadingStateChanged;
  // subscription_transitionServiceOnStart;
  // subscription_transitionServiceOnFinish;
  // subscription_transitionServiceOnSuccess;
  // subscription_transitionServiceOnError;
  // uiRouterTransitionStateVM: RouterTransitionStateVM;
  // constructor(private uiRouter: UIRouter) {
  //   this.uiRouter = uiRouter;
  //   this.onLoadingStateChanged = new EventEmitter(true);
  //   this.onAsyncLoadingStateChanged = new EventEmitter(true);
  //   this.updateLoadingStateChangedAndFire(this.uiRouter.globals.transition);
  //   this.subscription_transitionServiceOnStart = this.uiRouter.transitionService.onStart({}, () => {
  //     this.updateLoadingStateChangedAndFire();
  //   });
  //   this.subscription_transitionServiceOnFinish = this.uiRouter.transitionService.onFinish({}, () => {
  //     this.updateLoadingStateChangedAndFire();
  //   });
  //   this.subscription_transitionServiceOnSuccess = this.uiRouter.transitionService.onSuccess({}, () => {
  //     this.updateLoadingStateChangedAndFire();
  //   });
  //   this.subscription_transitionServiceOnError = this.uiRouter.transitionService.onError({}, () => {
  //     this.updateLoadingStateChangedAndFire();
  //   });
  // }
  // setLoadingManually(flag: boolean) {
  //   this.isLoadingManually = flag;
  //   this.updateLoadingStateChangedAndFire(null);
  // }
  // setLoadingAsync(flag: boolean, asyncLoadText: any = null) {
  //   this.onAsyncLoadingStateChanged.next({
  //     isLoadingAsync: flag,
  //     AsyncLoadText: asyncLoadText,
  //   });
  // }

  // setLoadingManuallyThisPromise(promise: Promise<any>) {
  //   this.setLoadingManually(true);
  //   return promise.then(
  //     (data) => {
  //       this.setLoadingManually(false);
  //       return data;
  //     },
  //     (error) => {
  //       this.setLoadingManually(false);
  //       return error;
  //     }
  //   );
  // }

  // setLoadingAsyncThisPromise(promise: Promise<any>, asyncLoadText: any = null) {
  //   this.setLoadingAsync(true, asyncLoadText);
  //   return promise.then(
  //     (data) => {
  //       this.setLoadingAsync(false);
  //       return data;
  //     },
  //     (error) => {
  //       this.setLoadingAsync(false);
  //       return error;
  //     }
  //   );
  // }
  // getLoadingStateVM() {
  //   return this.uiRouterTransitionStateVM;
  // }
  // updateLoadingStateChangedAndFire(: Transition) {
  //   // LINK: https://ui-router.github.io/ng1/docs/latest/classes/transition.transition-1.html#success
  //   //       "The value will be undefined if the transition is not complete"
  //   const isLoadingUiRouter =  && .success === undefined;
  //   // HACK? or not? Assumption: only the very first UiRouter transition will have from.name == ''
  //   const isLoadingUiRouterFirst =  && .from().name === '';
  //   this.uiRouterTransitionStateVM = {
  //     isRouteLoading: !!(isLoadingUiRouter || this.isLoadingManually),
  //     isFirstRouteLoading: !!(isLoadingUiRouter && isLoadingUiRouterFirst),
  //     ,
  //   };
  //   this.onLoadingStateChanged.next(this.uiRouterTransitionStateVM);
  // }
}
