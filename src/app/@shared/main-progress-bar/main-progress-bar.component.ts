import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ProgressBarService, RouterTransitionStateVM } from './main-progress-bar.service';

@Component({
  selector: 'app-main-progress-bar',
  templateUrl: './main-progress-bar.component.html',
  styleUrls: ['./main-progress-bar.component.scss'],
})
export class MainProgressBarComponent implements OnInit, OnDestroy {
  isRouteLoading;
  isFirstRouteLoading;
  subscriptiononLoadingStateChanged: Subscription;
  progressBarService: ProgressBarService;
  constructor(private _progressBarService: ProgressBarService) {
    this.progressBarService = _progressBarService;
    this.isRouteLoading = false;
    this.isFirstRouteLoading = false;
    this.subscriptiononLoadingStateChanged = this.progressBarService.onLoadingStateChanged.subscribe((transStateVM) => {
      this.updateUI(transStateVM);
    });
  }

  ngOnInit(): void {
    this.updateUI(this.progressBarService.getLoadingStateVM());
  }
  ngOnDestroy() {
    this.subscriptiononLoadingStateChanged.unsubscribe();
  }
  updateUI(transStateVM: RouterTransitionStateVM) {
    this.isRouteLoading = transStateVM.isRouteLoading;
    this.isFirstRouteLoading = transStateVM.isFirstRouteLoading;
  }
}
