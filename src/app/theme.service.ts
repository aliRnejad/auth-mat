import { EventEmitter, Injectable, Output } from '@angular/core';
import { Logger } from './@shared';
import { Theme } from './theme.model';

const log = new Logger('ThemeService');

@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  public toggleTheme = true;
  public availableThemes: Theme[] = [
    {name: 'main', class: ''},
    { name: 'secondary', class: 'my-light-theme' },
    { name: 'dark', class: 'my-dark-theme'}
  ]
  public myTheme: Theme = {
    class: 'my-light-theme'
  };

  @Output() themeChanged = new EventEmitter<boolean>();

  constructor() { }

  get theme(): boolean {
    return this.toggleTheme;
  }

  toggle() {
    this.toggleTheme = !this.toggleTheme;
    this.themeChanged.emit(this.theme);
  }

  changeTheme() {
    log.warn('theme changing', this);
    this.toggle();
  }

}

