import { Component, OnInit, ViewChild } from '@angular/core';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import { MatSidenav } from '@angular/material/sidenav';
import { filter, map, take } from 'rxjs/operators';

import { UntilDestroy, untilDestroyed } from '@shared';
import { ThemeService } from '../theme.service';

@UntilDestroy()
@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss'],
})
export class ShellComponent implements OnInit {
  theme: boolean;
  @ViewChild('sidenav', { static: false }) sidenav!: MatSidenav;

  constructor(private media: MediaObserver,
    private themeService: ThemeService) {
    this.theme = this.themeService.theme;
    themeService.themeChanged.subscribe(res => {
      this.theme = res;
    });
  }

  ngOnInit() {
    this.theme = this.themeService.theme;
      // Automatically close side menu on screens > sm breakpoint
    this.media
      .asObservable()
      .pipe(
        filter((changes: MediaChange[]) =>
          changes.some((change) => change.mqAlias !== 'xs' && change.mqAlias !== 'sm')
        ),
        untilDestroyed(this)
      )
      .subscribe(() => this.sidenav.close());
  }
}
