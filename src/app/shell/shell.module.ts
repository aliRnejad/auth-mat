import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { I18nModule } from '@app/i18n';
import { AuthModule } from '@app/auth';
import { ShellComponent } from './shell.component';
import { HeaderComponent } from './header/header.component';
import { SharedModule } from '@app/@shared';

@NgModule({
  imports: [ AuthModule, I18nModule, RouterModule, SharedModule],
  declarations: [HeaderComponent, ShellComponent],
})
export class ShellModule {}
