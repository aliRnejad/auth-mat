import { QuoteService } from './quote.service';
import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HomeResolver implements Resolve<string> {
  constructor(private quoteService: QuoteService) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string> {


    return this.quoteService
      .getRandomQuote({ category: 'explicit' });


    // let obs = new Observable<boolean>((sub) => {
    //   setTimeout(() => {
    //     sub.next(true);
    //     sub.complete();
    //   }, 1000);
    // })
    // return obs;
  }
}
